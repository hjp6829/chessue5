// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/enums.h"
#include "Tile.generated.h"
//UENUM(BlueprintType)
//enum class EPieceColor :uint8
//{
//	EPieceColorNone UMETA(DisplayName = "PieceColorNone"),
//	EPieceColorBlack UMETA(DisplayName = "PieceColorBlack"),
//	EPieceColorWhite UMETA(DisplayName = "PieceColorWhite"),
//};
UCLASS()
class CHESS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void SetTileColor(EPieceColor colorType);
	void Initialize(int32 H, int32 W);
	FVector GetTileSize();
	FIntVector2 GetTilePos();
private:
	FIntVector2 tilePos;
	FLinearColor tileColor;
	UMaterialInstanceDynamic* cubeMaterial;
	UStaticMeshComponent* cubeComp;
};
