// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
public:

private:
	UPROPERTY(meta=(BindWidget))
	class UButton* SingleButton;
	UPROPERTY(meta=(BindWidget))
	class UButton* LoadButton;
	UPROPERTY(meta=(BindWidget))
	class UMainLoadPanel* LoadPanel;
private:
	UFUNCTION()
	void OnClickSingle();
	UFUNCTION()
	void OnClickLoad();
};
