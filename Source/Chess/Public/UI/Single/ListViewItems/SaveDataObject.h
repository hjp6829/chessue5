// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "SaveDataObject.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API USaveDataObject : public UObject
{
	GENERATED_BODY()
public:
	void SetSaveData(FSaveData* data);
	FSaveData* GetSaveData();
	FString GetSaveName();
	class UBasePanel* parentPanel;
private:
	FSaveData* saveData;
};
