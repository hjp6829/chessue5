// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "UI/Single/BaseUI.h"
#include "SingleMenu.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API USingleMenu : public UBaseUI
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
public:
	UPROPERTY(BlueprintReadWrite, Category = "SubUI")
	class USavePanel* savePanel;
	UPROPERTY(BlueprintReadWrite, Category = "SubUI")
	class ULoadPanel* loadPanel;
protected:
	UPROPERTY(meta=(BindWidget))
	UButton* SaveMenuButton;
	UPROPERTY(meta=(BindWidget))
	UButton* LoadMenuButton;
	UPROPERTY(meta=(BindWidget))
	UButton* MainMenuButton;
private:
	UFUNCTION()
	void SaveButtinClick();
	UFUNCTION()
	void LoadButtinClick();
	UFUNCTION()
	void MainMenuButtinClick();


};
