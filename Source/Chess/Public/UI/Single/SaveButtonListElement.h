// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Managers/GameDataInstance.h"
#include "SaveButtonListElement.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API USaveButtonListElement : public UUserWidget,public IUserObjectListEntry
{
	GENERATED_BODY()
	public:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject)override;
	virtual ~USaveButtonListElement();
	void ResetSaveStruct();
private:
	UPROPERTY(meta=(BindWidget))
	class UTextBlock* saveNameText;
	UPROPERTY(meta = (BindWidget))
	class UButton* SaveButton;
	UFUNCTION()
	void OnClickSaveButton();
	class USaveDataObject* saveClass;
};
