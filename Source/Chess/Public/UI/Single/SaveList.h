// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SaveList.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API USaveList : public UUserWidget
{
	GENERATED_BODY()
	private:
	UPROPERTY(meta=(BindWidget))
	TObjectPtr<class UListView> SaveView;
public:
	UFUNCTION(BlueprintCallable,Category="Custom")
	void AddNewSave(UObject* save);
	void removeAll();
};
