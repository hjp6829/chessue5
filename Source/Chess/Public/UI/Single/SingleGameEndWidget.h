// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/Single/BaseUI.h"
#include "SingleGameEndWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API USingleGameEndWidget : public UBaseUI
{
	GENERATED_BODY()
private:
UPROPERTY(meta=(BindWidget))
class UTextBlock* WinTEXT;
UPROPERTY(meta=(BindWidget))
class UButton* BackToMenuButton;
UFUNCTION()
void BackToMenu();
public:
 virtual void NativeConstruct()override;
 void SetWinText(EPieceColor winColor);
};
