// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/Single/BaseUI.h"
#include "PawnPromotionWidget.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UPawnPromotionWidget : public UBaseUI
{
	GENERATED_BODY()
public:
	void SetPiecePos(FIntVector2 pos);
	virtual void NativeConstruct()override;
	UFUNCTION()
	void OnButtinClick();
	UFUNCTION(BlueprintCallable,Category="Custom")
	void SetPieceType(EPieceType type);
	virtual void Close()override;
private:
	UPROPERTY(meta = (BindWidget))
	class UButton* QueenPromotionButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* KnightPromotionButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* BishopPromotionButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* RookPromotionButton;
	EPieceType newPieceType = EPieceType::EPieceNone;
	FIntVector2 piecePos;
};
