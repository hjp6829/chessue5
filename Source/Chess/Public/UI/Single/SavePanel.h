// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UI/Single/BasePanel.h"
#include "SavePanel.generated.h"
/**
 * 
 */

UCLASS()
class CHESS_API USavePanel : public UBasePanel
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;
	virtual void InitializePanel()override;	

private:
	UPROPERTY(meta=(BindWidget))
	class UButton* SaveButton;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SaveName;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SaveTime;
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* SaveNameTextBox;
		UPROPERTY(meta = (BindWidget))
	class UImage* saveImage;
private:
UFUNCTION()
 void OnClickSaveButton();
 void SetTest(FSaveData* data);
};
