// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/Single/BasePanel.h"
#include "MainLoadPanel.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UMainLoadPanel : public UBasePanel
{
	GENERATED_BODY()
	public:
	virtual void NativeConstruct() override;
	virtual void InitializePanel()override;	
private:
	UPROPERTY(meta=(BindWidget))
	class UButton* LoadButton;
		UPROPERTY(meta=(BindWidget))
	class UButton* CancleButton;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SaveName;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SaveTime;
	UPROPERTY(meta = (BindWidget))
	class UImage* saveImage;
	FSaveData* SelectedData=nullptr;
	private:
	UFUNCTION()
	void OnClickLoadButton();
	void BindLoadData(FSaveData* data);
};
