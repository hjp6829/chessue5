// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Interfaces/enums.h>
#include "UIContainer.generated.h"

UCLASS()
class CHESS_API AUIContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUIContainer();
	class UBaseUI* GetUI(EUIType type);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
UPROPERTY(EditAnywhere,Category="Uis")
	TArray<TSubclassOf<class UBaseUI>> UIsClass;
	TArray<class UBaseUI*> UIs;
};
