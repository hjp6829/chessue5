// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Interfaces/enums.h"
#include "ChessModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API AChessModeBase : public AGameModeBase
{
	GENERATED_BODY()
	public:
#pragma region Getter Setter
		void SetChessTurn(EPieceColor turn);
		EPieceColor GetChessTurn();
		void ChangeTrun();
#pragma endregion
protected:
	virtual void BeginPlay() override;
	private:
		EPieceColor ChessTurn;
};
