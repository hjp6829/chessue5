// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InputActionValue.h"
#include "ChessPlayer.generated.h"

UCLASS()
class CHESS_API AChessPlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AChessPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
	APlayerController* playerController;
	UPROPERTY(EditAnywhere,Category=Input)
	class UInputMappingContext* defaultContext;
	UPROPERTY(EditAnywhere, Category = Input)
	class UInputAction* mouseClick;
	UPROPERTY(EditAnywhere, Category = Input)
	TArray<FIntVector2> canMovePos;
	UPROPERTY(EditAnywhere, Category = Trace)
	TArray<TEnumAsByte<ECollisionChannel>> traceLis;
	class APiece* selectedPiece;
private:
void OnMouseClick(const FInputActionValue& Value);
void makePieceMovePos(FIntVector2 tilePos);
void AttakEnemyPiece(FIntVector2 tilePos);
void SelectTile(FHitResult Result);
bool CheckAttack(FIntVector2 tilePos);
bool CheckMovePos(FIntVector2 tilePos);
void UpdateTile(FIntVector2 tilePos);
public:	

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
