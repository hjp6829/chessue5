#pragma once
#include "enums.generated.h"
UENUM(BlueprintType)
enum class EPieceColor :uint8
{
	EPieceColorNone UMETA(DisplayName = "PieceColorNone"),
	EPieceColorBlack UMETA(DisplayName = "PieceColorBlack"),
	EPieceColorWhite UMETA(DisplayName = "PieceColorWhite"),
};
UENUM(BlueprintType)
enum class EPieceType :uint8
{
	EPieceNone UMETA(DisplayName = "PieceTypeNone"),
	EPieceKing UMETA(DisplayName = "PieceTypeKing"),
	EPiecePawn UMETA(DisplayName = "PieceTypePawn"),
	EPieceQueen UMETA(DisplayName = "PieceTypeQueen"),
	EPieceKnight UMETA(DisplayName = "PieceTypeKnight"),
	EPieceBishop UMETA(DisplayName = "PieceTypeBishop"),
	EPieceRook UMETA(DisplayName = "PieceTypeRook"),
};
UENUM(BlueprintType)
enum class EUIType :uint8
{
	ESingleMenu UMETA(DisplayName = "SingleMenuUI"),
	EPawnPromotionUI UMETA(DisplayName = "PawnPromotion"),
	EGameEnd UMETA(DisplayName = "GameEnd"),
};