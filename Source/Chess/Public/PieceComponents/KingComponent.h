// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PieceComponents/PieceComponent.h"
#include "KingComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHESS_API UKingComponent : public UPieceComponent
{
	GENERATED_BODY()
public:
	virtual TArray<FIntVector2> MoveLocation() override;
	virtual TArray<FIntVector2>  GetMovePos() override;
	virtual TArray<FIntVector2> GetAttackPos()override;
	bool CheckCastling(const FIntVector2& kingPos, const FIntVector2& rookPos,const TSet<FIntVector2>& attackedPos);
	void MakeCastlingPos(FIntVector2 kingPos, FIntVector2 rookPos);
	FIntVector2 GetCastlingPos();
	FIntVector2 GetCastlingRookPos();
private:
	FIntVector2 castlingPos;
	FIntVector2 castlingRookPos;
};
