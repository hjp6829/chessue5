// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PieceComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHESS_API UPieceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPieceComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	bool CheckIDX(TSet<int>& removeIDX, TSet<int>& checkList,int idx,int dirNum);
	bool IsValidPosition(FIntVector2 pos);
		UFUNCTION()
	virtual TArray<FIntVector2> MoveLocation() PURE_VIRTUAL(UPieceComponent::MoveLocation,return TArray<FIntVector2>(););
protected:
   	class APiece* ownerPiece;
	class UGameDataInstance* dataManager;
	UPROPERTY()
	TArray<FIntVector2> blockFrendlyVector;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION()
	virtual TArray<FIntVector2> GetMovePos() PURE_VIRTUAL(UPieceComponent::GetMovePos,return TArray<FIntVector2>(););
	UFUNCTION()
	virtual TArray<FIntVector2> GetAttackPos()PURE_VIRTUAL(UPieceComponent::GetAttackPos,return TArray<FIntVector2>(););
	virtual void OnMoveEnd(FIntVector2 pos){}
	
};
