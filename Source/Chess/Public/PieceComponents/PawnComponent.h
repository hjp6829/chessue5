// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PieceComponents/PieceComponent.h"
#include "PawnComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHESS_API UPawnComponent : public UPieceComponent
{
	GENERATED_BODY()
public:
	virtual TArray<FIntVector2> MoveLocation() override;
	virtual TArray<FIntVector2>  GetMovePos() override;
	virtual TArray<FIntVector2> GetAttackPos()override;
	virtual void OnMoveEnd(FIntVector2 pos)override;
protected:
	int forwardDirection;
};
