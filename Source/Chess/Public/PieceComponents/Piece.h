// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/enums.h"
#include "Piece.generated.h"

UCLASS()
class CHESS_API APiece : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APiece();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FIntVector2 tilePos;
	bool isActive;
public:	
	void MovePieceObject(FVector newPos);
public:
	UPROPERTY(EditAnywhere,Category = "PieceData")
	EPieceType PieceType;
	void SetPiecePos(int32 H,int32 W);
	FIntVector2 GetPiecePos();
	bool GetisAvtive();
	void SetIsActive(bool value);
	void OnMoveEnd();
};
