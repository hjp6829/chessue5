// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.h"
#include "Interfaces/Serviec.h"
#include "Managers/GameDataInstance.h"
#include "BoardManager.generated.h"

UCLASS()
class CHESS_API ABoardManager : public AActor , public IServiec
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoardManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void Register() override;
	void SetLoadPiece();
	APiece* MakePiece(EPieceType type, EPieceColor color,FVector piecePos, FIntVector2 pos);
private:
		UPROPERTY(EditAnywhere)
		TSubclassOf<ATile> tileActor;
		class UGameDataInstance* dataManager;
private:
		TSharedPtr<TArray<FVector>> MakeChessBoard(FVector CentorPos);
		void SetPieceToBoard(TSharedPtr<TArray<FVector>> array);
};
