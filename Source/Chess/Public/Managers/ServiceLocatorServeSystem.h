// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "ServiceLocatorServeSystem.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UServiceLocatorServeSystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
private:
	TMap<UClass*, UObject*> Services;
public:
	template<typename T>
	T* GetService();
	template<typename T>
	bool Register(T* Service);
	void RemoveNullServices();
};
template<typename T>
inline bool UServiceLocatorServeSystem::Register(T* Service)
{
	UClass* serviceType = T::StaticClass();
	if (Services.Contains(serviceType))
	{
		UE_LOG(LogTemp, Warning, TEXT("Add false %s"), *serviceType->GetName());
		return false;
	}
	Services.Add(serviceType, Service);
	UE_LOG(LogTemp, Warning, TEXT("AddService : %s"), *serviceType->GetName());
	UE_LOG(LogTemp, Warning, TEXT("manager number : %d"), Services.Num());
	return true;
}
template<typename T>
inline T* UServiceLocatorServeSystem::GetService()
{
	UClass* serviceType = T::StaticClass();
	if (Services.Contains(serviceType))
	{
		return Cast<T>(Services[serviceType]);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s Can't Find Service."), *serviceType->GetName());
		return nullptr;
	}
}