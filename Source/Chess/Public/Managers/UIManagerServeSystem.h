// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include <Interfaces/enums.h>
#include "UI/UIContainer.h"
#include "UIManagerServeSystem.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API UUIManagerServeSystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	void SetUIContainer(class AUIContainer* Container);
	virtual void Initialize(FSubsystemCollectionBase& Collection)override;
	template<typename T>
	T* GetUI(EUIType uitype);
private:

	class AUIContainer* uiContainer;
};

template<typename T>
inline T* UUIManagerServeSystem::GetUI(EUIType uitype)
{
	class UBaseUI* baseTemp = uiContainer->GetUI(uitype);
	return Cast<T>(baseTemp);
}