// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "LoadingModeBase.generated.h"

/**
 * 
 */
DECLARE_DELEGATE(FOnLevelLoadCompleted)
UCLASS()
class CHESS_API ALoadingModeBase : public AGameModeBase
{
	GENERATED_BODY()
private:
	UPROPERTY(EditAnywhere,Category="UI")
	TSubclassOf<class UUserWidget> loadingWidgetClass;
	UPROPERTY()
	class UUserWidget* loadingWidget;
	static FString levelName;
private:
	//UFUNCTION()
	//void OnLevelLoad();
public:
	virtual void BeginPlay()override;
	static void LoadLevel(FString LevelName);
	static void LoadLevel(FString LevelName, FOnLevelLoadCompleted action);
	void Load();
	static FOnLevelLoadCompleted LevelLoadCompletedDelegate;
};
