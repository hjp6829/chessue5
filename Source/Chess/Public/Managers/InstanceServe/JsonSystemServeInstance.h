// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "PieceComponents/Piece.h"
#include "Managers/GameDataInstance.h"
#include "JsonSystemServeInstance.generated.h"

#pragma region SaveStruct
USTRUCT()
struct FSaveArry
{
	GENERATED_BODY()
public:
	UPROPERTY()
	int32 HeightIDX=0;
	UPROPERTY()
	TArray<FTileData> data;

};

USTRUCT()
struct FSaveData
{
	GENERATED_BODY()
public:
	UPROPERTY()
	FString saveName=TEXT("");
	UPROPERTY()
	FString saveTime = TEXT("");
	UPROPERTY()
	EPieceColor ownerColor=EPieceColor::EPieceColorNone;
	UPROPERTY()
	EPieceColor chessTurnColor= EPieceColor::EPieceColorNone;
	UPROPERTY()
	int32 saveIDX=0;
	UPROPERTY()
	TArray<FSaveArry> tiledata;
	void SetArry(const TMap<int32, FTileDataList>& data)
	{
		for (int32 i = 0; i < 8; i++)
		{
			FSaveArry saveArry;
			saveArry.HeightIDX = i;
			for (int32 j = 0; j < 8; j++)
			{
				saveArry.data.Add(data[i].pieceList[j]);
			}
			tiledata.Add(saveArry);
		}
	}
	TMap<int32, FTileDataList> LoadArry()
	{
		TMap<int32, FTileDataList> chessTileDatas;
		for (int32 i = 0; i < tiledata.Num(); i++)
		{
			FSaveArry* savearray= &tiledata[i];
			FTileDataList dataList;
			for (int32 j = 0; j < 8; j++)
			{
				FTileData saveFileData= savearray->data[j];
				dataList.AddNewData(saveFileData);
			}
			chessTileDatas.Add(i, dataList);
		}
		return chessTileDatas;
	}
};
USTRUCT()
struct FSaveListData
{
	GENERATED_BODY()
public:
	UPROPERTY()
	int32 NewSaveNum=0;
	UPROPERTY()
	TArray<FSaveData> list;
	void AddNewSvae(FSaveData data)
	{
		if (list.Num() != 0)
			NewSaveNum = list[list.Num() - 1].saveIDX;
		else
			NewSaveNum = 0;
		data.saveIDX = ++NewSaveNum;
		list.Add(data);
	}
	//void Deletedata(FString saveName)
	//{
	//	list.Remove(*GetSavedData(saveName));
	//}
	bool isContain(FString saveName)
	{
		bool bExists= list.ContainsByPredicate([&](FSaveData& data){
			return data.saveName==saveName;
		});
		return bExists;
	}
	FSaveData* GetSavedData(FString saveName)
	{
		FSaveData* foundSaveData = nullptr;
		FSaveData* foundSaveDataPtr = list.FindByPredicate([&](FSaveData& data)
			{
				return data.saveName == saveName;
			});

		if (foundSaveDataPtr)
		{
			foundSaveData = foundSaveDataPtr;
		}

		return foundSaveData;
	}
	void UpdateSaveData(FSaveData data)
	{
		FSaveData* temp = GetSavedData(data.saveName);
		temp->saveName = data.saveName;
		temp->saveTime = data.saveTime;
		temp->ownerColor = data.ownerColor;
		temp->saveIDX = data.saveIDX;
		temp->tiledata = data.tiledata;
	}
};
#pragma endregion
/**
 * 
 */
UCLASS()
class CHESS_API UJsonSystemServeInstance : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	void LoadGame(FString name);
	void SaveGameData(FString name);
	void LoadGameData(FString name);
	void LoadJsonData();
	TArray<FSaveData>* GetSaveDataList();
	FString GetFormattedDateTime();
	virtual void Initialize(FSubsystemCollectionBase& Collection)override;

private:
	UPROPERTY()
	FSaveListData saveList;
};
