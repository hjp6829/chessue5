// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interfaces/enums.h"
#include "PieceComponents/Piece.h"
#include "GameDataInstance.generated.h"
#pragma region structs
USTRUCT()
struct FTileData
{
	GENERATED_BODY()
public:
UPROPERTY()
	FVector tilePos= FVector::Zero();
	UPROPERTY()
	EPieceType pieceType=EPieceType::EPieceNone;
	UPROPERTY()
	EPieceColor pieceColor=EPieceColor::EPieceColorNone;
	UPROPERTY()
	APiece* pieceObject=nullptr;
	UPROPERTY()
	bool isActive=false;
	void DestroyPieceObject() 
	{
		if(pieceObject!= nullptr)
		{
			pieceObject->Destroy();
			pieceObject= nullptr;
		}
	}
};
USTRUCT()
struct FTileDataList
{
	GENERATED_BODY()
public:
	TArray<FTileData> pieceList;
	FTileData* GetTileData(int32 idx)
	{
		return &(pieceList[idx]);
	}
	void AddNewData(FTileData data)
	{
		pieceList.Add(data);
	}
	void SetTileData(int32 idx, FTileData* data)
	{
		pieceList[idx].tilePos= data->tilePos;
		pieceList[idx].pieceColor=data->pieceColor;
		pieceList[idx].pieceType= data->pieceType;
		pieceList[idx].pieceObject=data->pieceObject;
	}
	void ResetTile()
	{
		for (int32 i = 0; i < pieceList.Num(); i++)
		{
			pieceList[i].pieceColor = EPieceColor::EPieceColorNone;
			pieceList[i].pieceType = EPieceType::EPieceNone;
			pieceList[i].DestroyPieceObject();
		}
	}
};
USTRUCT()
struct FPieceList
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<APiece>> pieceList;
	TSubclassOf<APiece>* getPiece(EPieceType piecetype)
	{
		TSubclassOf<APiece>* FoundClss = pieceList.FindByPredicate([&](const TSubclassOf<APiece>& Element)
			{
				APiece* TempPiece = Element->GetDefaultObject<APiece>();
				if (TempPiece)
				{
					return TempPiece->PieceType == piecetype;
				}
				return false;
			});
		return FoundClss;
	}
};
USTRUCT()
struct FTilePiece
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	TArray<EPieceType> pieceList;
	EPieceType GetPieceType(int idx)
	{
		return pieceList[idx];
	}
};
#pragma endregion
/**
 * 
 */
UCLASS()
class CHESS_API UGameDataInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UGameDataInstance();
	~UGameDataInstance() {
		moveTileList.Empty();
	};
public:
	FTilePiece* GetTileWidth(int32 idx);
	TSubclassOf<APiece>* GetPieceObjectData(EPieceColor color, EPieceType type);
	APiece* GetTilePiece(FIntVector2 tilePos);
	EPieceColor GetTilePieceColor(FIntVector2 pos);
	EPieceType GetTilePieceType(FIntVector2 pos);
	TSet<FIntVector2> CheckAttackPos(EPieceColor color);
	TArray<APiece*> GetCastlingRooks(EPieceColor color);
	FVector GetTilePos(FIntVector2 pos);
	TMap<int32, FTileDataList>* GetTileDataMap();

	bool CheckSelectColor(FIntVector2 tilePos);
	bool IsCastling(FTileData* ExistingData, FTileData* NewData);

	void SetPawnPromotion(EPieceType type,FIntVector2 pos);
	void SetTileData(FIntVector2 tileIDX, EPieceType type, EPieceColor color, FVector tilePos, APiece* piece);
	void SetTileData(FIntVector2 tileIDX, APiece* piece);
	void UpdateTile(FIntVector2 ExistingLocation, FIntVector2 NewLocation);
	void MakeMoveTile(TArray<FIntVector2> canMovePos);
	void DestroyAndEmptyMoveTile();
	void DestroyPieceObject(FIntVector2 tilePos);
	void MovePiece(FIntVector2 ExistingLocation, FIntVector2 NewLocation);
		UFUNCTION()
		void ScreenShot(FString imageName);
	void SettTileDataMap(TMap<int32, FTileDataList> data);
	static bool IsLoad;
	EPieceColor loadTurn; 
	class UTexture2D* ImageFileLoad(FString name);
protected:
	virtual void Init() override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> moveTile;

		UPROPERTY(EditAnywhere)
	TMap<EPieceColor, FPieceList> PieceObject;
	UPROPERTY(EditAnywhere)
	TMap<int32, FTilePiece> TilePieceData;
	TMap<int32, FTileDataList> tileDataMap;
	UPROPERTY()
	TArray<AActor*> moveTileList;
};
