// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_API AMainMenuModeBase : public AGameModeBase
{
	GENERATED_BODY()
private:
 virtual void BeginPlay()override;
private:
UPROPERTY(EditAnywhere,Category="UI")
TSubclassOf<class UUserWidget> mainMenuClass;
UPROPERTY()
class UUserWidget* mainMenu;
};
