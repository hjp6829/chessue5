// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/UIContainer.h"
#include "UI/Single/BaseUI.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameDataInstance.h"
#include "Managers/UIManagerServeSystem.h"
// Sets default values
AUIContainer::AUIContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

UBaseUI* AUIContainer::GetUI(EUIType type)
{
	UBaseUI** tempUI = UIs.FindByPredicate([&](UBaseUI* ui)
		{
			return ui && ui->GetUIType() == type;
		});
		return *tempUI;
}

// Called when the game starts or when spawned
void AUIContainer::BeginPlay()
{
	Super::BeginPlay();
	for (int32 i = 0; i < UIsClass.Num(); i++)
	{
		UIs.Add(CreateWidget<UBaseUI>(GetWorld(), UIsClass[i]));
	}
	for (int32 i = 0; i < UIs.Num(); i++)
	{
		if(UIs[i]->GetStartView())
			UIs[i]->Open();
	}
	UGameInstance* instanceTemp = UGameplayStatics::GetGameInstance(GetWorld());
	UUIManagerServeSystem* uimanager = instanceTemp->GetSubsystem<UUIManagerServeSystem>();
	uimanager->SetUIContainer(this);
}

