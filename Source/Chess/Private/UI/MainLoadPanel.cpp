// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MainLoadPanel.h"
#include "Kismet/GameplayStatics.h"
#include "UI/Single/ListViewItems/SaveDataObject.h"
#include "UI/Single/SaveList.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "Managers/LoadingModeBase.h"
#include "Managers/ServiceLocatorServeSystem.h"
#include "Managers/BoardManager.h"
#include "Managers/GameDataInstance.h"
#include "Components/Image.h"
void UMainLoadPanel::InitializePanel()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	TArray<FSaveData>* datas = jsonmanager->GetSaveDataList();
	for (int32 i = 0; i < datas->Num(); i++)
	{
		USaveDataObject* temp = NewObject<USaveDataObject>();
		FSaveData* dataTemp = &((*datas)[i]);
		temp->SetSaveData(dataTemp);
		temp->parentPanel = this;
		saveList->AddNewSave(temp);
	}
}

void UMainLoadPanel::OnClickLoadButton()
{
	if (SelectedData == nullptr)
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	FString saveName = SelectedData->saveName;
	ALoadingModeBase::LoadLevel("SingleMap");
	jsonmanager->LoadGameData(saveName);
	SelectedData = nullptr;
	UGameDataInstance::IsLoad=true;
}
void UMainLoadPanel::BindLoadData(FSaveData* data)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* datainstance = Cast<UGameDataInstance>(gameInstance);
	SelectedData = data;
	SaveName->SetText(FText::FromString(data->saveName));
	SaveTime->SetText(FText::FromString(data->saveTime));
	UTexture2D* saveTexture = datainstance->ImageFileLoad(data->saveName);
	saveImage->SetBrushFromTexture(saveTexture);
}
void UMainLoadPanel::NativeConstruct()
{
	Super::NativeConstruct();
	if (LoadButton)
		LoadButton->OnClicked.AddDynamic(this, &UMainLoadPanel::OnClickLoadButton);
	onSaveSelected.BindUObject(this, &UMainLoadPanel::BindLoadData);
}