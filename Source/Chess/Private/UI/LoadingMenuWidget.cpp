// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/LoadingMenuWidget.h"
#include "Kismet/GameplayStatics.h"
void ULoadingMenuWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if(!isLoadingStart)
		return;
	 ULevelStreaming* streamingLevel=UGameplayStatics::GetStreamingLevel(this, *levelName);
	 if(streamingLevel)
	 UE_LOG(LogTemp, Warning, TEXT("sd1"));
}

void ULoadingMenuWidget::SetLevelName(FString name)
{
	levelName=name;
	isLoadingStart=true;
	UE_LOG(LogTemp, Warning, TEXT("sd"));
}
