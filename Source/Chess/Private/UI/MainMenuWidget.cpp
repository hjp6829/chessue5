// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MainMenuWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/LoadingModeBase.h"
#include "UI/MainLoadPanel.h"
#include "Managers/GameDataInstance.h"
void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();
	if (SingleButton)
	{
		UE_LOG(LogTemp, Warning, TEXT("Single1"));
		SingleButton->OnClicked.AddDynamic(this,&UMainMenuWidget::OnClickSingle);
	}
	if (LoadButton)
	{
		LoadButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OnClickLoad);
	}
}

void UMainMenuWidget::OnClickSingle()
{
	ALoadingModeBase::LoadLevel("SingleMap");
	UGameDataInstance::IsLoad = false;
}

void UMainMenuWidget::OnClickLoad()
{
	LoadPanel->SetVisibility(ESlateVisibility::Visible);
	LoadPanel->InitializePanel();
}
