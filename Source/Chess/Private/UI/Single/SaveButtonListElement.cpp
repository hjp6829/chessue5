// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/SaveButtonListElement.h"
#include "Components/TextBlock.h"
#include "UI/Single/ListViewItems/SaveDataObject.h"
#include "Components/Button.h"
#include "UI/Single/SavePanel.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
void USaveButtonListElement::NativeOnListItemObjectSet(UObject* ListItemObject)
{	
	saveClass =Cast<USaveDataObject>(ListItemObject);
	FString name= saveClass->GetSaveName();
	saveNameText->SetText(FText::FromString(name));
	if (SaveButton)
	{
		SaveButton->OnClicked.AddDynamic(this,&USaveButtonListElement::OnClickSaveButton);
	}
}

USaveButtonListElement::~USaveButtonListElement()
{
	UE_LOG(LogTemp,Warning,TEXT("Button Destroy"));
}

void USaveButtonListElement::ResetSaveStruct()
{
	saveClass=nullptr;
}

void USaveButtonListElement::OnClickSaveButton()
{
	saveClass->parentPanel->onSaveSelected.ExecuteIfBound(saveClass->GetSaveData());
}
