// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/SavePanel.h"
#include "UI/Single/SaveList.h"
#include "UI/Single/ListViewItems/SaveDataObject.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextBlock.h"
#include "Components/EditableTextBox.h"
#include "Components/Image.h"
void USavePanel::NativeConstruct()
{
	if(SaveButton)
		SaveButton->OnClicked.AddDynamic(this, &USavePanel::OnClickSaveButton);
	onSaveSelected.BindUObject(this,&USavePanel::SetTest);
}
void USavePanel::InitializePanel()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	TArray<FSaveData>*  datas = jsonmanager->GetSaveDataList();
	for (int32 i = 0; i < datas->Num(); i++)
	{
		USaveDataObject* temp = NewObject<USaveDataObject>();
		FSaveData* dataTemp= &((*datas)[i]);
		temp->SetSaveData(dataTemp);
		temp->parentPanel=this;
		saveList->AddNewSave(temp);
	}
}


void USavePanel::OnClickSaveButton()
{
	FText saveNameTemp = SaveNameTextBox->Text;
	SaveNameTextBox->SetText(FText::FromString(""));
	FString saveName= saveNameTemp.ToString();
	if(saveName=="")
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	jsonmanager->SaveGameData(saveName);
	ResetListView();
	InitializePanel();
}

void USavePanel::SetTest(FSaveData* data)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* datainstance = Cast<UGameDataInstance>(gameInstance);
	SaveName->SetText(FText::FromString(data->saveName));
	SaveTime->SetText(FText::FromString(data->saveTime));
	UTexture2D* saveTexture = datainstance->ImageFileLoad(data->saveName);
	saveImage->SetBrushFromTexture(saveTexture);
}
