// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/SaveList.h"
#include "Components/ListView.h"

void USaveList::AddNewSave(UObject* save)
{
	SaveView->AddItem(save);
}

void USaveList::removeAll()
{
	SaveView->ClearListItems();
}
