// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/SingleMenu.h"
#include "Managers/GameDataInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/LoadingModeBase.h"
#include "UI/Single/SavePanel.h"
#include "UI/Single/LoadPanel.h"
void USingleMenu::NativeConstruct()
{
	Super::NativeConstruct();
	if(SaveMenuButton)
	{
		SaveMenuButton->OnClicked.AddDynamic(this,&USingleMenu::SaveButtinClick);
	}
	if (LoadMenuButton)
	{
		LoadMenuButton->OnClicked.AddDynamic(this, &USingleMenu::LoadButtinClick);
	}
	if (MainMenuButton)
	{
		MainMenuButton->OnClicked.AddDynamic(this, &USingleMenu::MainMenuButtinClick);
	}
}

void USingleMenu::SaveButtinClick()
{
	savePanel->InitializePanel();
}

void USingleMenu::LoadButtinClick()
{
	loadPanel->InitializePanel();
}

void USingleMenu::MainMenuButtinClick()
{
	ALoadingModeBase::LoadLevel("MainMenu");
}

