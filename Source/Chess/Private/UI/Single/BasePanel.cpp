// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/BasePanel.h"
#include "UI/Single/SaveList.h"
void UBasePanel::SetSaveList(USaveList* savelist)
{
	saveList = savelist;
}

void UBasePanel::ResetListView()
{
	saveList->removeAll();
}
