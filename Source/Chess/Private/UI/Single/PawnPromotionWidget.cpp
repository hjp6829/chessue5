// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/PawnPromotionWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameDataInstance.h"
void UPawnPromotionWidget::SetPiecePos(FIntVector2 pos)
{
	piecePos=pos;
}

void UPawnPromotionWidget::NativeConstruct()
{
	if (QueenPromotionButton)
	{
		QueenPromotionButton->OnClicked.AddDynamic(this,&UPawnPromotionWidget::OnButtinClick);
	}
	if (KnightPromotionButton)
	{
		KnightPromotionButton->OnClicked.AddDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	}
	if (BishopPromotionButton)
	{
		BishopPromotionButton->OnClicked.AddDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	}
	if (RookPromotionButton)
	{
		RookPromotionButton->OnClicked.AddDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	}
}

void UPawnPromotionWidget::OnButtinClick()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* datainstance = Cast<UGameDataInstance>(gameInstance);
	datainstance->SetPawnPromotion(newPieceType, piecePos);
	Close();
}

void UPawnPromotionWidget::SetPieceType(EPieceType type)
{
	newPieceType = type;
}

void UPawnPromotionWidget::Close()
{
	Super::Close();
	QueenPromotionButton->OnClicked.RemoveDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	KnightPromotionButton->OnClicked.RemoveDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	BishopPromotionButton->OnClicked.RemoveDynamic(this, &UPawnPromotionWidget::OnButtinClick);
	RookPromotionButton->OnClicked.RemoveDynamic(this, &UPawnPromotionWidget::OnButtinClick);
}
