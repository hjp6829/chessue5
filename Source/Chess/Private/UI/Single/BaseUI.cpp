// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/BaseUI.h"

void UBaseUI::Open()
{
	this->AddToViewport();
}

void UBaseUI::Close()
{
	this->RemoveFromViewport();
}

EUIType UBaseUI::GetUIType()
{
	return uitype;
}

bool UBaseUI::GetStartView()
{
	return isAddViewStart;
}
