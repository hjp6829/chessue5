// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/SingleGameEndWidget.h"
#include "Managers/LoadingModeBase.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void USingleGameEndWidget::BackToMenu()
{
	ALoadingModeBase::LoadLevel("MainMenu");
}

void USingleGameEndWidget::NativeConstruct()
{
	Super::NativeConstruct();
	if (BackToMenuButton)
	{
		BackToMenuButton->OnClicked.AddDynamic(this,&USingleGameEndWidget::BackToMenu);
	}
}

void USingleGameEndWidget::SetWinText(EPieceColor winColor)
{
	FString wintext="";
	if(winColor==EPieceColor::EPieceColorBlack)
		{wintext="WinBlack";}
	else if(winColor == EPieceColor::EPieceColorWhite)
	{
		wintext = "WinWhite";
	}
	if(WinTEXT)
		WinTEXT->SetText(FText::FromString(wintext));
}
