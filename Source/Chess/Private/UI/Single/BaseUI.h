// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include <Interfaces/enums.h>
#include "BaseUI.generated.h"

/**
 * 
 */
UCLASS()
class UBaseUI : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void Open();
	virtual void Close();
	EUIType GetUIType();
	bool GetStartView();
private: 
	UPROPERTY(EditAnywhere,Category="Type")
	EUIType uitype;
	UPROPERTY(EditAnywhere,Category="Setting")
	bool isAddViewStart;
};
