// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/LoadPanel.h"
#include "Kismet/GameplayStatics.h"
#include "UI/Single/ListViewItems/SaveDataObject.h"
#include "UI/Single/SaveList.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "Components/Image.h"
void ULoadPanel::InitializePanel()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	TArray<FSaveData>* datas = jsonmanager->GetSaveDataList();
	for (int32 i = 0; i < datas->Num(); i++)
	{
		USaveDataObject* temp = NewObject<USaveDataObject>();
		FSaveData* dataTemp = &((*datas)[i]);
		temp->SetSaveData(dataTemp);
		temp->parentPanel = this;
		saveList->AddNewSave(temp);
	}
}

void ULoadPanel::OnClickLoadButton()
{
	if(SelectedData==nullptr)
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UJsonSystemServeInstance* jsonmanager = gameInstance->GetSubsystem<UJsonSystemServeInstance>();
	FString saveName = SelectedData->saveName;
	jsonmanager->LoadGame(saveName);
	SelectedData=nullptr;
}
void ULoadPanel::BindLoadData(FSaveData* data)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* datainstance = Cast<UGameDataInstance>(gameInstance);
	SelectedData=data;
	SaveName->SetText(FText::FromString(data->saveName));
	SaveTime->SetText(FText::FromString(data->saveTime));
	UTexture2D* saveTexture = datainstance->ImageFileLoad(data->saveName);
	saveImage->SetBrushFromTexture(saveTexture);
}
void ULoadPanel::NativeConstruct()
{
	if (LoadButton)
		LoadButton->OnClicked.AddDynamic(this, &ULoadPanel::OnClickLoadButton);
	onSaveSelected.BindUObject(this, &ULoadPanel::BindLoadData);
}
