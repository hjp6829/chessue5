// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Single/ListViewItems/SaveDataObject.h"

void USaveDataObject::SetSaveData(FSaveData* data)
{
	saveData = data;
}

FSaveData* USaveDataObject::GetSaveData()
{
	return saveData;
}

FString USaveDataObject::GetSaveName()
{
	return saveData->saveName;
}