// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Managers/GameDataInstance.h"
#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "BasePanel.generated.h"

/**
 * 
 */
DECLARE_DELEGATE_OneParam(FOnSaveSelected, FSaveData*)
UCLASS()
class UBasePanel : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category="Custom")
	void SetSaveList(USaveList* savelist);
			UFUNCTION()
	virtual void InitializePanel() PURE_VIRTUAL(UBasePanel::InitializePanel,);
	UFUNCTION(BlueprintCallable, Category="Custom")
	void ResetListView();
public:
	FOnSaveSelected onSaveSelected;

protected:
	class USaveList* saveList;

};
