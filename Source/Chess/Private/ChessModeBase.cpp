// Fill out your copyright notice in the Description page of Project Settings.
#include "ChessModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Managers/BoardManager.h"
#include "Managers/GameDataInstance.h"
#include "Kismet/GameplayStatics.h"
void AChessModeBase::BeginPlay()
{	
	Super::BeginPlay();
	if(!UGameDataInstance::IsLoad)
	{ChessTurn = EPieceColor::EPieceColorWhite;}
	else
	{
		UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
		UGameDataInstance* dataManager = Cast<UGameDataInstance>(gameInstance);
		ChessTurn= dataManager->loadTurn;
	}
}
void AChessModeBase::SetChessTurn(EPieceColor turn) { ChessTurn = turn; }

EPieceColor AChessModeBase::GetChessTurn() { return ChessTurn; }

void AChessModeBase::ChangeTrun() { ChessTurn = (ChessTurn == EPieceColor::EPieceColorWhite) ? EPieceColor::EPieceColorBlack : EPieceColor::EPieceColorWhite; }
