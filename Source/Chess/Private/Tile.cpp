// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATile::SetTileColor(EPieceColor colorType)
{
	if (cubeMaterial)
	{
		switch (colorType)
		{
		case EPieceColor::EPieceColorBlack:
			cubeMaterial->SetVectorParameterValue(FName("Color"), FLinearColor::Black);
			break;
		case EPieceColor::EPieceColorWhite:
			cubeMaterial->SetVectorParameterValue(FName("Color"), FLinearColor::White);
			break;
		default:
			break;
		}
	}
}

void ATile::Initialize(int32 H, int32 W)
{
	cubeComp = FindComponentByClass<UStaticMeshComponent>();
	if (cubeComp)
	{
		cubeMaterial = cubeComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, cubeComp->GetMaterial(0));
	}
	tilePos = FIntVector2(H, W);
}

FVector ATile::GetTileSize()
{
	FVector scale = cubeComp->GetComponentScale();
	return scale;
}

inline FIntVector2 ATile::GetTilePos(){return tilePos;}

