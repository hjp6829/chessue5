// Fill out your copyright notice in the Description page of Project Settings.
#include "PieceComponents/PieceComponent.h"
#include "Managers/GameDataInstance.h"
#include "Kismet/GameplayStatics.h"
#include "PieceComponents/Piece.h"


// Sets default values for this component's properties
UPieceComponent::UPieceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPieceComponent::BeginPlay()
{
	Super::BeginPlay();
	ownerPiece=Cast<APiece>(GetOwner());
	// ...
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	dataManager = Cast<UGameDataInstance>(gameInstance);
}

bool UPieceComponent::CheckIDX(TSet<int>& removeIDX, TSet<int>& checkList, int idx, int dirNum)
{
	bool isContain=false;
	for (int i : checkList)
	{
		if (i % dirNum == idx % dirNum)
		{
			removeIDX.Add(idx);
			isContain=true;
			break;
		}
	}
	return isContain;
}

bool UPieceComponent::IsValidPosition(FIntVector2 pos)
{
	int boardSize=8;
	return (pos.X>=0&& pos.X< boardSize)&&(pos.Y>=0&&pos.Y< boardSize);
}


// Called every frame
void UPieceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

