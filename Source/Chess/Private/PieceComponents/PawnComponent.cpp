// Fill out your copyright notice in the Description page of Project Settings.
#include "PieceComponents/PawnComponent.h"
#include "Managers/GameDataInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/UIManagerServeSystem.h"
#include "UI/Single/PawnPromotionWidget.h"
TArray<FIntVector2> UPawnComponent::MoveLocation()
{
	TArray<FIntVector2> location;
	FIntVector2 piecePos = ownerPiece->GetPiecePos();
	EPieceColor colorTemp = dataManager->GetTilePieceColor(piecePos);
	forwardDirection=(colorTemp==EPieceColor::EPieceColorWhite)?1:-1;
	location.Add(FIntVector2(piecePos.X + forwardDirection + 0, piecePos.Y + 1));
	location.Add(FIntVector2(piecePos.X + forwardDirection + 0, piecePos.Y - 1));
	return location;
}
TArray<FIntVector2> UPawnComponent::GetAttackPos()
{
    TArray<FIntVector2> movePos = MoveLocation();
    for (int32 i = movePos.Num() - 1; i >= 0; i--)
    {
        if (!IsValidPosition(movePos[i]))
            movePos.RemoveAt(i);
    }
    return movePos;
}
void UPawnComponent::OnMoveEnd(FIntVector2 pos)
{
    if (pos.X == 0 || pos.X == 7)
    {
        UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
        UUIManagerServeSystem* uimanager = gameInstance->GetSubsystem<UUIManagerServeSystem>();
        UPawnPromotionWidget* promotionWidget = uimanager->GetUI<UPawnPromotionWidget>(EUIType::EPawnPromotionUI);
        if (!promotionWidget)
        {
            UE_LOG(LogTemp, Warning, TEXT("PawnPromotionNull"));
            return;
        }
        promotionWidget->Open();
        promotionWidget->SetPiecePos(pos);
    }
}
TArray<FIntVector2> UPawnComponent::GetMovePos()
{
	TArray<FIntVector2> movePos = GetAttackPos();
	FIntVector2 piecePos = ownerPiece->GetPiecePos();
	if (!ownerPiece->GetisAvtive())
	{
		movePos.Add(FIntVector2(piecePos.X + forwardDirection + forwardDirection, piecePos.Y));
		movePos.Add(FIntVector2(piecePos.X + forwardDirection, piecePos.Y));
	}
	else
		movePos.Add(FIntVector2(piecePos.X + forwardDirection, piecePos.Y));
    FIntVector2 directionOffsets[] = { FIntVector2(0, 0), FIntVector2(0, 1), FIntVector2(0, -1) };
    FIntVector2 twoStepsForwardPos = FIntVector2(piecePos.X + forwardDirection + forwardDirection + directionOffsets[0].X, piecePos.Y + directionOffsets[0].Y);
    int32 directionOffsetsLength=sizeof(directionOffsets)/sizeof(directionOffsets[0]);
    for (int32 i = 0; i < directionOffsetsLength; i++)
    {
        FIntVector2 oneStepForwardPos = FIntVector2(piecePos.X + forwardDirection + directionOffsets[i].X, piecePos.Y + directionOffsets[i].Y);
        if (!IsValidPosition(oneStepForwardPos))
        {
            continue;
        }

        EPieceColor oneStepForwardPosColor = dataManager->GetTilePieceColor(oneStepForwardPos);
        if (oneStepForwardPosColor != EPieceColor::EPieceColorNone)
        {
            EPieceColor colorTemp = dataManager->GetTilePieceColor(piecePos);
            if (i == 0)
            {
                movePos.Remove(oneStepForwardPos);
                if (!ownerPiece->GetisAvtive())
                    movePos.Remove(twoStepsForwardPos);
            }
            else if (oneStepForwardPosColor == colorTemp)
            {
                movePos.Remove(oneStepForwardPos);
            }
        }

        if (!ownerPiece->GetisAvtive())
        {
            EPieceColor twoStepsForwardPosColor = dataManager->GetTilePieceColor(twoStepsForwardPos);
            if (twoStepsForwardPosColor != EPieceColor::EPieceColorNone)
                movePos.Remove(twoStepsForwardPos);
        }
        if (oneStepForwardPosColor == EPieceColor::EPieceColorNone)
        {
            if (i > 0)
                movePos.Remove(oneStepForwardPos);
        }
    }
	return movePos;
}