// Fill out your copyright notice in the Description page of Project Settings.
#include "PieceComponents/KnightComponent.h"
#include "Managers/GameDataInstance.h"
#include "PieceComponents/Piece.h"

TArray<FIntVector2> UKnightComponent::MoveLocation()
{
	TArray<FIntVector2> location;
	FIntVector2 piecePos = ownerPiece->GetPiecePos();
	location.Add(FIntVector2(piecePos.X+1, piecePos.Y + 2));
	location.Add(FIntVector2(piecePos.X + 2, piecePos.Y + 1));
	location.Add(FIntVector2(piecePos.X+2, piecePos.Y - 1));
	location.Add(FIntVector2(piecePos.X + 1, piecePos.Y-2));
	location.Add(FIntVector2(piecePos.X-1, piecePos.Y -2));
	location.Add(FIntVector2(piecePos.X -2, piecePos.Y - 1));
	location.Add(FIntVector2(piecePos.X-2, piecePos.Y +1));
	location.Add(FIntVector2(piecePos.X -1, piecePos.Y+2));
	return location;
}
TArray<FIntVector2> UKnightComponent::GetAttackPos()
{
	TArray<FIntVector2> movePos = MoveLocation();
	TSet<int32> removeIDX;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (!IsValidPosition(movePos[i]))
		{
			removeIDX.Add(i);
			continue;
		}
		EPieceColor pieceColor = dataManager->GetTilePieceColor(ownerPiece->GetPiecePos());
		EPieceColor selectColor = dataManager->GetTilePieceColor(movePos[i]);
		if (pieceColor == selectColor)
		{
			blockFrendlyVector.Add(movePos[i]);
		}
	}
	TArray<FIntVector2> resultTemp;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (!removeIDX.Contains(i))
		{
			resultTemp.Add(movePos[i]);
		}
	}
	return resultTemp;
}
TArray<FIntVector2> UKnightComponent::GetMovePos()
{
	TArray<FIntVector2> resultTemp = GetAttackPos();
	TArray<FIntVector2> result;
	for (int32 i = 0; i < resultTemp.Num(); i++)
	{
		if (!blockFrendlyVector.Contains(resultTemp[i]))
		{
			result.Add(resultTemp[i]);
		}
	}
	blockFrendlyVector.Empty();
	return result;
}
