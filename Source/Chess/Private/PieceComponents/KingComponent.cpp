// Fill out your copyright notice in the Description page of Project Settings.
#include "PieceComponents/KingComponent.h"
#include "Managers/GameDataInstance.h"
#include "PieceComponents/Piece.h"


TArray<FIntVector2> UKingComponent::MoveLocation()
{
	TArray<FIntVector2> location;
	FIntVector2 piecePos = ownerPiece->GetPiecePos();
	location.Add(FIntVector2(piecePos.X+1, piecePos.Y + 1));
	location.Add(FIntVector2(piecePos.X -1, piecePos.Y - 1));
	location.Add(FIntVector2(piecePos.X+1, piecePos.Y -1));
	location.Add(FIntVector2(piecePos.X-1, piecePos.Y+1));
	location.Add(FIntVector2(piecePos.X, piecePos.Y + 1));
	location.Add(FIntVector2(piecePos.X, piecePos.Y - 1));
	location.Add(FIntVector2(piecePos.X + 1, piecePos.Y));
	location.Add(FIntVector2(piecePos.X - 1, piecePos.Y));
	return location;
}
TArray<FIntVector2> UKingComponent::GetAttackPos()
{
	TArray<FIntVector2> movePos = MoveLocation();
	TSet<int32> removeIDX;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (!IsValidPosition(movePos[i]))
		{
			removeIDX.Add(i);
			continue;
		}
		EPieceColor pieceColor = dataManager->GetTilePieceColor(ownerPiece->GetPiecePos());
		EPieceColor selectColor = dataManager->GetTilePieceColor(movePos[i]);
		if (pieceColor == selectColor)
		{
			removeIDX.Add(i);
		}
	}
	TArray<FIntVector2> result;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (!removeIDX.Contains(i))
		{
			result.Add(movePos[i]);
		}
	}
	return result;
}
bool UKingComponent::CheckCastling(const FIntVector2& kingPos, const FIntVector2& rookPos, const TSet<FIntVector2>& attackedPos)
{
	int32 dis=rookPos.Y-kingPos.Y;
	int32 checkCount=dis<0?-1:1;
	dis=FMath::Abs(dis);
	int32 y= kingPos.Y+checkCount;
	int32 CASTLING_RANGE = 2;
	EPieceType pieceType =dataManager->GetTilePieceType(FIntVector2(kingPos.X,y));
	for (int32 i = 0; i < dis - 1; i++)
	{
		if (pieceType != EPieceType::EPieceNone)
			return false;
		y += checkCount;
	}
	y = kingPos.Y + checkCount;
	for (int i = 0; i < CASTLING_RANGE; i++)
	{
		if (attackedPos.Contains(FIntVector2(kingPos.X, y)))
			return false;
		y += checkCount;
	}
	return true;
}
void UKingComponent::MakeCastlingPos(FIntVector2 kingPos, FIntVector2 rookPos)
{
	int32 dis = rookPos.Y - kingPos.Y;
	int32 checkCount = dis < 0 ? -1 : 1;
	castlingPos = FIntVector2(kingPos.X, kingPos.Y + checkCount + checkCount);
	castlingRookPos = FIntVector2(castlingPos.X, castlingPos.Y - checkCount);
}
FIntVector2 UKingComponent::GetCastlingPos(){return castlingPos;}
FIntVector2 UKingComponent::GetCastlingRookPos(){return castlingRookPos;}
TArray<FIntVector2> UKingComponent::GetMovePos()
{
	TArray<FIntVector2> movePos = GetAttackPos();
	EPieceColor pieceColor = dataManager->GetTilePieceColor(ownerPiece->GetPiecePos());
	TSet<FIntVector2> attackedPos=dataManager->CheckAttackPos(pieceColor);
	if(ownerPiece->GetisAvtive()|| attackedPos.Contains(ownerPiece->GetPiecePos()))
		return movePos;
	TArray<APiece*> rooks=dataManager->GetCastlingRooks(pieceColor);
	if(rooks.Num()==0)
		return movePos;
	for (int i = rooks.Num() - 1; i >= 0; i--)
	{
		if (!CheckCastling(ownerPiece->GetPiecePos(), rooks[i]->GetPiecePos(), attackedPos))
			rooks.RemoveAt(i);
	}
	if (rooks.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("2"));
		for(APiece* piece : rooks)
			movePos.Add(piece->GetPiecePos());
	}
	return movePos;
}

