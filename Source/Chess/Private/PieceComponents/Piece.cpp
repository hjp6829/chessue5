// Fill out your copyright notice in the Description page of Project Settings.


#include "PieceComponents/Piece.h"
#include "PieceComponents/PieceComponent.h"
// Sets default values
APiece::APiece()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void APiece::BeginPlay()
{
	Super::BeginPlay();
	
}

void APiece::MovePieceObject(FVector newPos)
{
	SetActorLocation(newPos);
}

void APiece::SetPiecePos(int32 H, int32 W)
{
	tilePos=FIntVector2(H,W);
}

FIntVector2 APiece::GetPiecePos()
{
	return tilePos;
}
void APiece::OnMoveEnd()
{
	UActorComponent* tempComp = GetComponentByClass(UPieceComponent::StaticClass());
	if(!tempComp)
		return;
	UPieceComponent* pieceComp = Cast<UPieceComponent>(tempComp);
	pieceComp->OnMoveEnd(tilePos);
}
bool APiece::GetisAvtive(){return isActive;}

void APiece::SetIsActive(bool value){isActive=value;}



