// Fill out your copyright notice in the Description page of Project Settings.
#include "PieceComponents/QueenComponent.h"
#include "Managers/GameDataInstance.h"
#include "PieceComponents/Piece.h"


TArray<FIntVector2> UQueenComponent::MoveLocation()
{
	TArray<FIntVector2> location;
	FIntVector2 piecePos = ownerPiece->GetPiecePos();
	for (int32 i = 1; i < 8; i++)
	{
		location.Add(FIntVector2(piecePos.X + i, piecePos.Y + i));
		location.Add(FIntVector2(piecePos.X - i, piecePos.Y - i));
		location.Add(FIntVector2(piecePos.X + i, piecePos.Y - i));
		location.Add(FIntVector2(piecePos.X - i, piecePos.Y + i));
		location.Add(FIntVector2(piecePos.X, piecePos.Y + i));
		location.Add(FIntVector2(piecePos.X, piecePos.Y - i));
		location.Add(FIntVector2(piecePos.X + i, piecePos.Y));
		location.Add(FIntVector2(piecePos.X - i, piecePos.Y));
	}
	return location;
}
TArray<FIntVector2> UQueenComponent::GetAttackPos()
{
	TArray<FIntVector2> movePos = MoveLocation();
	TSet<int32> removeIDX;
	TSet<int32> blockEnemyIDX;
	TSet<int32> blockFrendlyIDX;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (CheckIDX(removeIDX, blockFrendlyIDX, i, 8))
			continue;
		if (CheckIDX(removeIDX, blockEnemyIDX, i, 8))
			continue;
		if (!IsValidPosition(movePos[i]))
		{
			removeIDX.Add(i);
			continue;
		}
		EPieceColor pieceColor = dataManager->GetTilePieceColor(ownerPiece->GetPiecePos());
		EPieceColor selectColor = dataManager->GetTilePieceColor(movePos[i]);
		if (pieceColor == selectColor)
		{
			blockFrendlyVector.Add(movePos[i]);
			blockFrendlyIDX.Add(i);
		}
		else if (selectColor != EPieceColor::EPieceColorNone && pieceColor != selectColor)
		{
			blockEnemyIDX.Add(i);
		}
	}
	TArray<FIntVector2> resultTemp;
	for (int32 i = 0; i < movePos.Num(); i++)
	{
		if (!removeIDX.Contains(i))
			resultTemp.Add(movePos[i]);
	}
	return resultTemp;
}
TArray<FIntVector2> UQueenComponent::GetMovePos()
{
	TArray<FIntVector2> resultTemp = GetAttackPos();
	TArray<FIntVector2> result;
	for (int32 i = 0; i < resultTemp.Num(); i++)
	{
		if (!blockFrendlyVector.Contains(resultTemp[i]))
		{
			result.Add(resultTemp[i]);
		}
	}
	blockFrendlyVector.Empty();
	return result;
}