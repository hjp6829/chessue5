// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/LoadingModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UI/LoadingMenuWidget.h"
#include "Managers/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"
FString ALoadingModeBase::levelName;
FOnLevelLoadCompleted ALoadingModeBase::LevelLoadCompletedDelegate;

void ALoadingModeBase::BeginPlay()
{	
	Super::BeginPlay();
	if(!loadingWidgetClass)
		return;
	loadingWidget=CreateWidget<UUserWidget>(GetWorld(), loadingWidgetClass);
	if(!loadingWidget)
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* ServiceLocatorManager = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	loadingWidget->AddToViewport();
	ULoadingMenuWidget* loadingMenu = Cast<ULoadingMenuWidget>(loadingWidget);
	loadingMenu->SetLevelName(levelName);
	Load();
	ServiceLocatorManager->RemoveNullServices();
}

void ALoadingModeBase::LoadLevel(FString LevelName)
{
	levelName= LevelName;
	UWorld* world=GEngine->GetWorldFromContextObject(GEngine->GameViewport, EGetWorldErrorMode::LogAndReturnNull);
	if (world)
	{
		UGameplayStatics::OpenLevel(world, "LoadingLevel");
	}
}

void ALoadingModeBase::LoadLevel(FString LevelName, FOnLevelLoadCompleted action)
{
	LevelLoadCompletedDelegate=action;
	LevelLoadCompletedDelegate.Execute();
	levelName = LevelName;
	UWorld* world = GEngine->GetWorldFromContextObject(GEngine->GameViewport, EGetWorldErrorMode::LogAndReturnNull);
	if (world)
	{
		UGameplayStatics::OpenLevel(world, "LoadingLevel");
	}
}

void ALoadingModeBase::Load()
{
	UWorld* world = GetWorld();
	if (world)
	{
		UGameplayStatics::OpenLevel(world, *levelName);
		UE_LOG(LogTemp, Warning, TEXT("LoadingComplite"));
		if (LevelLoadCompletedDelegate.IsBound())
		{
			UE_LOG(LogTemp, Warning, TEXT("Delegate"));
			LevelLoadCompletedDelegate.Execute();
		}
	}
}
//void ALoadingModeBase::Load(FString LevelName, FOnLevelLoadCompleted action)
//{
//	FLatentActionInfo LatentInfo;
//	LatentInfo.CallbackTarget = this;
//	LatentInfo.ExecutionFunction = FName("OnLevelLoad");
//	LatentInfo.Linkage = 0;
//	LatentInfo.UUID = FMath::Rand();
//
//	UGameplayStatics::LoadStreamLevel(this, FName(*LevelName), true, true, LatentInfo);
//	LevelLoadCompletedDelegate = action;
//}