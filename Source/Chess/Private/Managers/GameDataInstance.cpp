// Fill out your copyright notice in the Description page of Project Settings.
#include "Managers/GameDataInstance.h"
#include "PieceComponents/KingComponent.h"
#include "PieceComponents/PieceComponent.h"
#include "Managers/ServiceLocatorServeSystem.h"
#include "Managers/BoardManager.h"
#include "Kismet/GameplayStatics.h"
#include "ChessModeBase.h"
#include "Managers/UIManagerServeSystem.h"
#include "UI/Single/SingleGameEndWidget.h"
#include "IImageWrapperModule.h"
#include "IImageWrapper.h"
#include "Engine/Texture2D.h"
bool UGameDataInstance::IsLoad=false;

UGameDataInstance::UGameDataInstance()
{
	for (int32 i = 0; i < 8; i++)
	{
		FTileDataList dataList;
		for (int32 j = 0; j < 8; j++)
		{
			FTileData data;
			dataList.AddNewData(data);
		}
		tileDataMap.Add(i, dataList);
	}
}


TSubclassOf<APiece>* UGameDataInstance::GetPieceObjectData(EPieceColor color, EPieceType type)
{
	FPieceList* listTemp = PieceObject.Find(color);
	return 	listTemp->getPiece(type);
}

void UGameDataInstance::SetPawnPromotion(EPieceType type, FIntVector2 pos)
{
	FTileData* tileData = tileDataMap[pos.X].GetTileData(pos.Y);
	tileData->DestroyPieceObject();
	tileData->pieceType= type;
	UServiceLocatorServeSystem* servicemanager=GetSubsystem<UServiceLocatorServeSystem>();
	ABoardManager* boradmanager = servicemanager->GetService<ABoardManager>();
	APiece*  pieceTemp = boradmanager->MakePiece(type, tileData->pieceColor,tileData->tilePos, pos);
	tileData->pieceObject=pieceTemp;
}

void UGameDataInstance::SetTileData(FIntVector2 tileIDX, EPieceType type, EPieceColor color, FVector tilePos, APiece* piece)
{
	FTileData* tileData = tileDataMap[tileIDX.X].GetTileData(tileIDX.Y);
	tileData->pieceColor=color;
	tileData->pieceObject=piece;
	tileData->pieceType=type;
	tileData->tilePos= tilePos;
}

void UGameDataInstance::SetTileData(FIntVector2 tileIDX, APiece* piece)
{
	FTileData* tileData = tileDataMap[tileIDX.X].GetTileData(tileIDX.Y);
	tileData->pieceObject = piece;
}

void UGameDataInstance::UpdateTile(FIntVector2 ExistingLocation, FIntVector2 NewLocation)
{
	FTileData* ExistingTempData= tileDataMap[ExistingLocation.X].GetTileData(ExistingLocation.Y);
	FTileData* NewTempData = tileDataMap[NewLocation.X].GetTileData(NewLocation.Y);
	AGameModeBase* CurrentGameMode = UGameplayStatics::GetGameMode(GetWorld());
	AChessModeBase* singlebase = Cast<AChessModeBase>(CurrentGameMode);
	if(!IsCastling(ExistingTempData, NewTempData))
	{
		MovePiece(ExistingLocation, NewLocation);
		singlebase->ChangeTrun();
		return;
	}
	UActorComponent* desiredComp = ExistingTempData->pieceObject->GetComponentByClass(UPieceComponent::StaticClass());
	if (desiredComp)
	{
		UKingComponent* kingComp = Cast<UKingComponent>(desiredComp);
		if (kingComp)
		{
			kingComp->MakeCastlingPos(ExistingLocation, NewLocation);
			FIntVector2 castlingPos= kingComp->GetCastlingPos();
			FIntVector2 castlingRookPos = kingComp->GetCastlingRookPos();
			MovePiece(ExistingLocation, castlingPos);
			MovePiece(NewLocation, castlingRookPos);
			singlebase->ChangeTrun();
		}
	}
}
void UGameDataInstance::DestroyPieceObject(FIntVector2 tilePos)
{
	FTileData* destroyData = tileDataMap[tilePos.X].GetTileData(tilePos.Y);
	destroyData->DestroyPieceObject();
	destroyData->pieceColor=EPieceColor::EPieceColorNone;
	if (destroyData->pieceType == EPieceType::EPieceKing)
	{
		UUIManagerServeSystem* uimanager = GetSubsystem<UUIManagerServeSystem>();
		USingleGameEndWidget* singleEndUI = uimanager->GetUI<USingleGameEndWidget>(EUIType::EGameEnd);
		AGameModeBase* CurrentGameMode = UGameplayStatics::GetGameMode(GetWorld());
		AChessModeBase* singlebase = Cast<AChessModeBase>(CurrentGameMode);
		if(singleEndUI)
			{singleEndUI->Open();}
			else
			UE_LOG(LogTemp,Warning,TEXT("Null"));
		//singleEndUI->SetWinText(singlebase->GetChessTurn());
	}
	destroyData->pieceType= EPieceType::EPieceNone;

}
void UGameDataInstance::MovePiece(FIntVector2 ExistingLocation, FIntVector2 NewLocation)
{
	FTileData* ExistingTempData = tileDataMap[ExistingLocation.X].GetTileData(ExistingLocation.Y);
	FTileData* NewTempData = tileDataMap[NewLocation.X].GetTileData(NewLocation.Y);
	FVector NewPos = NewTempData->tilePos;
	ExistingTempData->pieceObject->SetActorLocation(NewPos);

	EPieceType tempType = ExistingTempData->pieceType;
	EPieceColor tempColor = ExistingTempData->pieceColor;
	APiece* pieceTemp = ExistingTempData->pieceObject;
	ExistingTempData->isActive = true;
	NewTempData->isActive = true;

	ExistingTempData->pieceType = NewTempData->pieceType;
	ExistingTempData->pieceColor = NewTempData->pieceColor;
	ExistingTempData->pieceObject = NewTempData->pieceObject;

	NewTempData->pieceType = tempType;
	NewTempData->pieceColor = tempColor;
	NewTempData->pieceObject = pieceTemp;
	pieceTemp->SetPiecePos(NewLocation.X, NewLocation.Y);
	pieceTemp->SetIsActive(true);
	pieceTemp->OnMoveEnd();
	DestroyAndEmptyMoveTile();
}

bool UGameDataInstance::IsCastling(FTileData* ExistingData, FTileData* NewData)
{	
	if (ExistingData->pieceColor == NewData->pieceColor)
	{
		if (ExistingData->pieceType == EPieceType::EPieceKing && NewData->pieceType == EPieceType::EPieceRook)
		{
			return true;
		}
	}
	return false;
}



void UGameDataInstance::SettTileDataMap(TMap<int32, FTileDataList> data)
{
	tileDataMap= data;
}

UTexture2D* UGameDataInstance::ImageFileLoad(FString name)
{
	FString FullFilePath= "D:/unreal/ScreenShot/" + name + ".png";
	UTexture2D* LoadedT2D = NULL;
	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);
	TArray<uint8> RawFileData;
	if (!FFileHelper::LoadFileToArray(RawFileData, *FullFilePath))
	{
		return nullptr;
	}
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{
		TArray<uint8>* UncompressedBRGA=new TArray<uint8>();
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, *UncompressedBRGA))
		{
			LoadedT2D = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_B8G8R8A8);
			if (!LoadedT2D)
			{
				return NULL;
			}
			void* TextureData = LoadedT2D->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(TextureData, UncompressedBRGA->GetData(), UncompressedBRGA->Num());
			LoadedT2D->PlatformData->Mips[0].BulkData.Unlock();
			LoadedT2D->UpdateResource();
		}
	}
	return LoadedT2D;
}

void UGameDataInstance::Init()
{
	Super::Init();
}

bool UGameDataInstance::CheckSelectColor(FIntVector2 tilePos)
{
	FTileData* dataTemp = tileDataMap[tilePos.X].GetTileData(tilePos.Y);
	AGameModeBase* CurrentGameMode = UGameplayStatics::GetGameMode(GetWorld());
	AChessModeBase* singlebase = Cast<AChessModeBase>(CurrentGameMode);
	if(dataTemp->pieceColor!= singlebase->GetChessTurn())
		return false;
	return true;
}

void UGameDataInstance::MakeMoveTile(TArray<FIntVector2> canMovePos)
{
	DestroyAndEmptyMoveTile();
	for (FIntVector2 Pos : canMovePos)
	{
		//UE_LOG(LogTemp, Warning, TEXT("PosX : %d PosY : %d"), Pos.X, Pos.Y);
		FTileData* dataTemp = tileDataMap[Pos.X].GetTileData(Pos.Y);
		AActor* tileTemp = GetWorld()->SpawnActor<AActor>(moveTile, dataTemp->tilePos, FRotator(0, 0, 0));
		moveTileList.Add(tileTemp);
	}
	
}

void UGameDataInstance::DestroyAndEmptyMoveTile()
{
	for (AActor* actor : moveTileList)
	{
		actor->Destroy();
	}
	moveTileList.Empty();
}


TSet<FIntVector2> UGameDataInstance::CheckAttackPos(EPieceColor color)
{
	TSet<FIntVector2> tempSet;
	for (int32 i = 0; i < 8; i++)
	{
		for (int32 j = 0; j < 8; j++)
		{
			FTileData* data = tileDataMap[i].GetTileData(j);
			if(data->pieceColor!= color&& data->pieceColor!=EPieceColor::EPieceColorNone)
			{
				UActorComponent* desiredComp = data->pieceObject->GetComponentByClass(UPieceComponent::StaticClass());
				if (desiredComp)
				{
					UPieceComponent* pieceComp = Cast<UPieceComponent>(desiredComp);
					if (pieceComp)
					{
						tempSet.Append(pieceComp->GetAttackPos());
					}
				}
			}
		}
	}
	return tempSet;
}



TArray<APiece*> UGameDataInstance::GetCastlingRooks(EPieceColor color)
{
	TArray<APiece*> tempList;
	for (int32 i = 0; i < 8; i++)
	{
		for (int32 j = 0; j < 8; j++)
		{
			FTileData* dataTemp= tileDataMap[i].GetTileData(j);
			if(dataTemp->pieceColor == color && dataTemp->pieceType == EPieceType::EPieceRook)
			{
				if (!dataTemp->isActive)
				{
					tempList.Add(dataTemp->pieceObject);
				}
			}
		}
	}
	return tempList;
}
void UGameDataInstance::ScreenShot(FString imageName)
{
	FScreenshotRequest::RequestScreenshot(FString("D:/unreal/ScreenShot/" + imageName + ".png"), false, false);
}
#pragma region inline
APiece* UGameDataInstance::GetTilePiece(FIntVector2 tilePos) { return tileDataMap[tilePos.X].GetTileData(tilePos.Y)->pieceObject; }

EPieceColor UGameDataInstance::GetTilePieceColor(FIntVector2 pos) { return tileDataMap[pos.X].GetTileData(pos.Y)->pieceColor; }

FVector UGameDataInstance::GetTilePos(FIntVector2 pos){return tileDataMap[pos.X].GetTileData(pos.Y)->tilePos;}

TMap<int32, FTileDataList>* UGameDataInstance::GetTileDataMap()
{
	return &tileDataMap;
}

EPieceType UGameDataInstance::GetTilePieceType(FIntVector2 pos) { return tileDataMap[pos.X].GetTileData(pos.Y)->pieceType; }

FTilePiece* UGameDataInstance::GetTileWidth(int32 idx) { return 	TilePieceData.Find(idx); }
#pragma endregion


