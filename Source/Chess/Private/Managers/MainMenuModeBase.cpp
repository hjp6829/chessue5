// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/MainMenuModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameDataInstance.h"
#include "Blueprint/UserWidget.h"
void AMainMenuModeBase::BeginPlay()
{
	Super::BeginPlay();
	if (mainMenuClass)
	{
		mainMenu=CreateWidget<UUserWidget>(GetWorld(),mainMenuClass);
		if(mainMenu)
			mainMenu->AddToViewport();
	}
}
