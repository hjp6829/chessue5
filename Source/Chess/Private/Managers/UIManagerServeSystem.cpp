// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/UIManagerServeSystem.h"

void UUIManagerServeSystem::SetUIContainer(AUIContainer* Container)
{
	uiContainer = Container;
}

void UUIManagerServeSystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	UE_LOG(LogTemp,Warning,TEXT("Initialize"));
}
