// Fill out your copyright notice in the Description page of Project Settings.
#include "Managers/BoardManager.h"
#include "Managers/GameDataInstance.h"
#include "Managers/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABoardManager::ABoardManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ABoardManager::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* gameInstance=UGameplayStatics::GetGameInstance(GetWorld());
	dataManager=Cast<UGameDataInstance>(gameInstance);
	if(!UGameDataInstance::IsLoad)
	{SetPieceToBoard(MakeChessBoard(GetActorLocation()));}
	else
	{
		MakeChessBoard(GetActorLocation());
		SetLoadPiece();
	}
	Register();
}
void ABoardManager::Register()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* ServiceLocatorManager = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	if(ServiceLocatorManager)
		ServiceLocatorManager->Register(this);
}
void ABoardManager::SetLoadPiece()
{
	if (!dataManager)
		return;
	TMap<int32, FTileDataList>* tiledata= dataManager->GetTileDataMap();
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			FTileDataList* tempStruct= tiledata->Find(i);
			FTileData* tempFile= tempStruct->GetTileData(j);
			EPieceType type = tempFile->pieceType;
			FVector tilePos = tempFile->tilePos;
			if (type == EPieceType::EPieceNone)
			{
				continue;
			}
			EPieceColor color= tempFile->pieceColor;
			TSubclassOf<APiece>* pieceTemp = dataManager->GetPieceObjectData(color, type);
			FRotator Rotator;
			//if (color == EPieceColor::EPieceColorWhite)
			//{
			//	Rotator = FRotator(0, 0, 0);
			//}
			//else
			//{
			//	Rotator = FRotator(0, 180,0);
			//}
			APiece* PieceInstance = GetWorld()->SpawnActor<APiece>(*pieceTemp, tilePos, Rotator);
			PieceInstance->SetPiecePos(i, j);
			dataManager->SetTileData(FIntVector2(i, j), PieceInstance);
		}
	}
}
APiece* ABoardManager::MakePiece(EPieceType type,  EPieceColor color,FVector piecePos, FIntVector2 pos)
{
	TSubclassOf<APiece>* pieceTemp = dataManager->GetPieceObjectData(color, type);
	APiece* PieceInstance = GetWorld()->SpawnActor<APiece>(*pieceTemp, piecePos, FRotator::ZeroRotator);
	PieceInstance->SetPiecePos(pos.X, pos.Y);
	return PieceInstance;
}
TSharedPtr<TArray<FVector>> ABoardManager::MakeChessBoard(FVector CentorPos)
{
	FVector installBoxCenter = CentorPos;
	FVector tileSize;
	TSharedPtr<TArray<FVector>>DynamicArray = MakeShared<TArray<FVector>>();
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			ATile* tileTemp = GetWorld()->SpawnActor<ATile>(tileActor, installBoxCenter, FRotator(0, 0, 0));
			tileTemp->Initialize(i, j);
			tileSize = tileTemp->GetTileSize();
			if ((j + i) % 2 == 0)
				tileTemp->SetTileColor(EPieceColor::EPieceColorBlack);
			else
				tileTemp->SetTileColor(EPieceColor::EPieceColorWhite);
			DynamicArray->Add(installBoxCenter);
			installBoxCenter.X -= (tileSize.X * 100);
		}
		installBoxCenter.Y += (tileSize.Y * 100);
		installBoxCenter.X = CentorPos.X;
	}
	return DynamicArray;
}

void ABoardManager::SetPieceToBoard(TSharedPtr<TArray<FVector>> array)
{	
	if(!dataManager)
		return;
	int idx = 0;
	for (int i = 0; i < 8; i++)
	{
		FTilePiece* piecetypes = dataManager->GetTileWidth(i);
		for (int j = 0; j < 8; j++)
		{
			EPieceType type = piecetypes->GetPieceType(j);
			if (type == EPieceType::EPieceNone)
			{
				dataManager->SetTileData(FIntVector2(i, j), EPieceType::EPieceNone, EPieceColor::EPieceColorNone, (*array)[idx], NULL);
				idx++;
				continue;
			}
			EPieceColor pieceColor = (i < 2) ? EPieceColor::EPieceColorWhite : EPieceColor::EPieceColorBlack;
			TSubclassOf<APiece>* pieceTemp = dataManager->GetPieceObjectData(pieceColor, type);
			FVector PosTemp = (*array)[idx];
			FRotator Rotator=FRotator::ZeroRotator;
			//if(pieceColor==EPieceColor::EPieceColorWhite)
			//{
			//	Rotator =FRotator(0,0,0);}
			//else
			//{Rotator = FRotator(0, 180, 0);}
			APiece* PieceInstance = GetWorld()->SpawnActor<APiece>(*pieceTemp, PosTemp, Rotator);
			PieceInstance->SetPiecePos(i, j);
			dataManager->SetTileData(FIntVector2(i, j), type, pieceColor, PosTemp, PieceInstance);
			idx++;
		}
	}
}



