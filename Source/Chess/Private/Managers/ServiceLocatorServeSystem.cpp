// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/ServiceLocatorServeSystem.h"

void UServiceLocatorServeSystem::RemoveNullServices()
{
	TArray<UClass*> KeysToRemove;
	for (auto& Elem : Services)
	{
		if(!IsValid(Elem.Value))
			KeysToRemove.Add(Elem.Key);
	}
	for (UClass* Key : KeysToRemove)
	{
		Services.Remove(Key);
	}
}
