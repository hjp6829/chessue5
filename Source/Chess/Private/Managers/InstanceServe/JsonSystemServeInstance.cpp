// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/InstanceServe/JsonSystemServeInstance.h"
#include "Managers/ServiceLocatorServeSystem.h"
#include "Managers/BoardManager.h"
#include "JsonObjectConverter.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"
#include "Kismet/GameplayStatics.h"
#include "ChessModeBase.h"
void UJsonSystemServeInstance::SaveGameData(FString name)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataInstance=Cast<UGameDataInstance>(gameInstance);
	AGameModeBase* GameMode = UGameplayStatics::GetGameMode(GetWorld());
	AChessModeBase* gameMode = Cast<AChessModeBase>(GameMode);
	FSaveData savedata;
	savedata.saveName = name;
	dataInstance->ScreenShot(savedata.saveName);
	savedata.saveTime = GetFormattedDateTime();
	savedata.ownerColor = EPieceColor::EPieceColorBlack;
	savedata.chessTurnColor = gameMode->GetChessTurn();
	TMap<int32, FTileDataList>* tileDataMap= dataInstance->GetTileDataMap();
	savedata.SetArry(*tileDataMap);
	if (saveList.isContain(name))
	{
		saveList.UpdateSaveData(savedata);
	}
	else
	{
		saveList.AddNewSvae(savedata);
	}
	FString outPut;
	FString filePath = "D:/unreal/SaveJsonFile/savedata.json";
	FJsonObjectConverter::UStructToJsonObjectString(FSaveListData::StaticStruct(), &saveList, outPut, 0, 0);
	bool writeSuccess = FFileHelper::SaveStringToFile(outPut, *filePath);
}
void UJsonSystemServeInstance::LoadGameData(FString name)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataInstance = Cast<UGameDataInstance>(gameInstance);
	FSaveData* dataTemp = saveList.GetSavedData(name);
	TMap<int32, FTileDataList> listTemp = dataTemp->LoadArry();
	dataInstance->loadTurn= dataTemp->chessTurnColor;
	dataInstance->SettTileDataMap(listTemp);
}
void UJsonSystemServeInstance::LoadGame(FString name)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataInstance = Cast<UGameDataInstance>(gameInstance);
	TMap<int32, FTileDataList>* tileDataMap = dataInstance->GetTileDataMap();
	UServiceLocatorServeSystem* ServiceLocatorManager = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	ABoardManager* Boardmanager = ServiceLocatorManager->GetService<ABoardManager>();
	if (!Boardmanager)
		UE_LOG(LogTemp, Error, TEXT("boardmanager is null"));
	for (auto& Elem : *tileDataMap)
	{
		Elem.Value.ResetTile();
	}
	LoadGameData(name);
	Boardmanager->SetLoadPiece();
}
void UJsonSystemServeInstance::LoadJsonData()
{
	FString LoadedString;
	FString filePath = "D:/unreal/SaveJsonFile/savedata.json";
	if (FFileHelper::LoadFileToString(LoadedString, *filePath))
	{
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(LoadedString);
		if (FJsonSerializer::Deserialize(Reader, JsonObject) && JsonObject.IsValid())
		{
			FSaveListData LoadedData;
			if (FJsonObjectConverter::JsonObjectToUStruct(JsonObject.ToSharedRef(), FSaveListData::StaticStruct(), &LoadedData, 0, 0))
			{
				saveList = LoadedData;
			}
		}
	}
}
FString UJsonSystemServeInstance::GetFormattedDateTime()
{
	const FDateTime now = FDateTime::Now();
	return now.ToString(TEXT("%m/%d/%Y %H:%M:%S"));
}
void UJsonSystemServeInstance::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	LoadJsonData();
}

TArray<FSaveData>* UJsonSystemServeInstance::GetSaveDataList() { return &saveList.list; }