// Fill out your copyright notice in the Description page of Project Settings.
#include "ChessPlayer.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "Tile.h"
#include "Managers/GameDataInstance.h"
#include "Kismet/GameplayStatics.h"
#include "PieceComponents/PieceComponent.h"
#include "PieceComponents/Piece.h"

// Sets default values
AChessPlayer::AChessPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AChessPlayer::BeginPlay()
{
	Super::BeginPlay();
	if (APlayerController* tempControllrt = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* SubSystem =
			ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(tempControllrt->GetLocalPlayer()))
			SubSystem->AddMappingContext(defaultContext, 0);
		playerController= tempControllrt;
	}
}

void AChessPlayer::OnMouseClick(const FInputActionValue& Value)
{
	FVector MouseLocation,MouseDirection;
	if(Value.Get<bool>())
	{
		playerController->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);
		FVector End= MouseLocation+MouseDirection*10000;

		FHitResult HitResult;
		FCollisionQueryParams Params;
		ECollisionChannel channel=ECC_GameTraceChannel1;
		bool Hit = GetWorld()->LineTraceSingleByChannel(HitResult, MouseLocation, End, channel, Params);
		if(!Hit)
		return;
		SelectTile(HitResult);
	}
}

void AChessPlayer::makePieceMovePos(FIntVector2 tilePos)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataManager = Cast<UGameDataInstance>(gameInstance);
	if(!dataManager->CheckSelectColor(tilePos))
		return;
	APiece* pieceTemp= dataManager->GetTilePiece(tilePos);
	if(pieceTemp==NULL)
		return;
	selectedPiece = pieceTemp;
	UActorComponent* desiredComp = pieceTemp->GetComponentByClass(UPieceComponent::StaticClass());
	if (desiredComp)
	{
		UPieceComponent* pieceComp = Cast<UPieceComponent>(desiredComp);
		if (pieceComp)
		{
			canMovePos = pieceComp->GetMovePos();
			dataManager->MakeMoveTile(canMovePos);
		}
	}

}

void AChessPlayer::AttakEnemyPiece(FIntVector2 tilePos)
{
	if(!CheckMovePos(tilePos))
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataManager = Cast<UGameDataInstance>(gameInstance);
	dataManager->DestroyPieceObject(tilePos);
	UpdateTile(tilePos);
}

void AChessPlayer::SelectTile(FHitResult Result)
{
	AActor* tempActor = Result.GetActor();
	ATile* tempTile = Cast<ATile>(tempActor);
	FIntVector2 tilePos = tempTile->GetTilePos();
	if(selectedPiece==NULL)
	{
		makePieceMovePos(tilePos);
		return;
	}
	else if(CheckAttack(tilePos))
	{
		AttakEnemyPiece(tilePos);
		return;
	}
	else if(CheckMovePos(tilePos))
	{
		UpdateTile(tilePos);
	}
	else
	{
		makePieceMovePos(tilePos);
	}
}

bool AChessPlayer::CheckAttack(FIntVector2 tilePos)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataManager = Cast<UGameDataInstance>(gameInstance);
	if(dataManager->GetTilePiece(tilePos) ==NULL)
	{
		return false;
	}
	if(dataManager->CheckSelectColor(tilePos))
	{
		return false;
	}
	return true;
}

bool AChessPlayer::CheckMovePos(FIntVector2 tilePos)
{
	return canMovePos.Contains(tilePos);
}

void AChessPlayer::UpdateTile(FIntVector2 tilePos)
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UGameDataInstance* dataManager = Cast<UGameDataInstance>(gameInstance);
	dataManager->UpdateTile(selectedPiece->GetPiecePos(), tilePos);
	selectedPiece = NULL;
}

// Called to bind functionality to input
void AChessPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(mouseClick, ETriggerEvent::Triggered, this, &AChessPlayer::OnMouseClick);
	}
}

